FROM microsoft/azure-cli:2.0.51

COPY pipe /usr/bin/

ENTRYPOINT ["/usr/bin/pipe.sh"]
